<?php

use app\modules\user\Module;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/email-confirm', 'token' => $user->email_confirm_token]);
?>

<?= 'Добрый день {username}', ['username' => $user->username]; ?>

<?= 'Ниже представлена ссылка для подтверждения Вашего EMAIL' ?>

<?= $confirmLink ?>

<?= 'Игнорировать регистрацию' ?>