<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'Панель Администратора',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => ['log'],
    'modules' => [
    ],
    'defaultRoute'        => 'order',
    'components'          => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                '/'                                     => 'order/index',
'<_a:(login|logout|signup|email-confirm|request-password-reset|password-reset)>' => 'site/<_a>',
                
                '<controller>/<action>/<id:\d+>'        => '<controller>/<action>', 
                '<controller>/<action>'                 => '<controller>/<action>', 

                '<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>'    => '<_c>/<_a>',
                '<_c:[\w\-]+>/<id:\d+>'                 => '<_c>/view',
                '<_c:[\w\-]+>'                          => '<_c>/index',
            ],
        ], 
        'request' => [
            'baseUrl' => '/admin'
        ],
        'user' => [
            'identityClass'   => 'backend\models\Admin',
            'enableAutoLogin' => true,

            'identityCookie'  => [
                'name'     => '_backendIdentity',
                'httpOnly' => true,
            ],
        ],
        'authManager' => [
            'class'          => 'yii\rbac\PhpManager',
            'defaultRoles'   => [ 'admin'],//'user',
            'itemFile'       => '@backend/rbac/data/items.php',
            'assignmentFile' => '@backend/rbac/data/assignments.php',
            'ruleFile'       => '@backend/rbac/data/rules.php'
        ],
        'session' => [
            'name' => 'BACKENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
//                'path' => '/admin',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
