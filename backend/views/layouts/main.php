<?php
use backend\assets\AppAsset;
use backend\components\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Панель ',//администратора
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [];
            if (Yii::$app->user->isGuest) {
//                $menuItems[] = ['label' => 'Регистрация',   'url' => ['/signup']];
                $menuItems[] = ['label' => 'Вход',          'url' => ['/login']];
            } else {
                $menuItems = [            
                    ['label' => 'На сайт',      'url' => '/'],
                    ['label' => 'Звонки',       'url' => ['/callback/index']],
                    ['label' => 'Подписка',     'url' => ['/subscribe/index']],
                    ['label' => 'Заказы',       'url' => ['/order/index']],
                    ['label' => 'Скидки',       'items' => [
                        ['label' => 'Скидки',       'url' => ['/rebate/index']],
                        ['label' => 'Купоны',       'url' => ['/coupon/index']],
                    ],],
                    ['label' => 'Товары',       'url' => ['/product/index']],
                    ['label' => 'Настройки',    'items' => [
                        ['label' => 'Страницы',         'url' => ['/page/index']],
                        ['label' => 'Слайдер',          'url' => ['/slider/index']],
                        ['label' => 'Баннеры',          'url' => ['/banner/index']],
                        ['label' => 'Ссылки соцсетей',  'url' => ['/soclink/index']],
                        ['label' => 'Яндекс карта',     'url' => ['/yamap/index']],
                        ['label' => 'Опции',            'url' => ['/option/index']],
                    ],],
                ];
                
                $menuItems[] = [
                    'label'       => 'Выйти (' . Yii::$app->user->identity->username . ')',
                    'url'         => ['/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items'   => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?= Alert::widget() ?>

            <?= $content ?>

        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; Debora Cosmetics <?= date('Y') ?></p>
        <!--<p class="pull-right"><?php // echo Yii::powered() ?></p>-->
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
