<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = HTML::encode('Создать слайд');
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-create">

    <h1><?= $this->title?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
