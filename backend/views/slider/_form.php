<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;


/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data'],
//        'enableAjaxValidation'   => true,
//        'enableClientValidation' => true,
            ]); ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>
                <?php echo $form->field($model, 'file')->fileInput() ?>
                <?php echo $form->field($model, 'page_id')->textInput() ?>
                <?php echo $form->field($model, 'status')->dropDownList(\common\models\Slider::getStatusesList()) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать слайд' : 'Изменить слайд', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>