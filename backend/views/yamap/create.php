<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Yamap */

$this->title = 'Создать Яндекс карту';
$this->params['breadcrumbs'][] = ['label' => 'Яндекс карта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yamap-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
