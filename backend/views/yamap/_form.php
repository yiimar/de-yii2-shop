<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Yamap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="yamap-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-5">

            <?= $form->field($model, 'page_id')->textInput() ?>

            <?= $form->field($model, 'url')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'sid')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'width')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'height')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'source_type')->textInput(['maxlength' => 100]) ?>

            <?php echo $form->field($model, 'status')->dropDownList(\common\models\Yamap::statusForDrop()); ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
