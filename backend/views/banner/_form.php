<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin([
                   'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?php echo $form->field($model, 'file')->fileInput() ?>
                <?= $form->field($model, 'width')->textInput() ?>
                <?= $form->field($model, 'height')->textInput() ?>
                <?= $form->field($model, 'class')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'place_page_id')->dropDownList(\common\models\Page::getParentList(), ['prompt'=>'Не определять']); ?>
                <?= $form->field($model, 'target_place_id')->dropDownList(\common\models\Page::getTotalList(),  ['prompt'=>'Не определять']); ?>
                <?= $form->field($model, 'target')->textInput(['maxlength' => true]) ?>
                <?php echo $form->field($model, 'status')->dropDownList(\common\models\Banner::getStatusesList()) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
