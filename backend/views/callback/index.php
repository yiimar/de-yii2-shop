<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\query\CallbackQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы звонков';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callback-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать заказ звонка', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'phone',
            'created:datetime',
            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return HTML::encode(\common\models\Callback::getStatusById($model->status));
                },
            ],
 
            [
                'class'         => 'yii\grid\ActionColumn',
                'header'        => 'Действия',
                'headerOptions' => ['width' => '40'],
                'template'      => '{update} {delete}',
            ],
        ],
    ]); ?>

</div>
