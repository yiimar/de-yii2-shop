<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Callback */

$this->title = 'Создать заказ звонка';
$this->params['breadcrumbs'][] = ['label' => 'Заказы звонков', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
