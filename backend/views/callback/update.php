<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Callback */

$this->title = 'Изменить заказ звонка: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы звонков', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] =$this->title ;
?>
<div class="callback-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
