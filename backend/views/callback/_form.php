<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Callback */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-lg-5">

        <div class="callback-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'phone')->textInput() ?>

            <?= $form->field($model, 'created')->textInput() ?>

            <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(\common\models\Callback::getStasusesList(), 'status', 'title')) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
