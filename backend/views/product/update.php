<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = 'Изменить товар: ' . ' ' . $model->title;

//echo HTML::img(Yii::$app->params['uploadPath'] . $model->photo, ['width'=>'200px', 'height'=>'auto']);

$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'      => $model,
    ]) ?>

</div>
