<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = Html::encode('Заказ #') . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">
    <div class="row">
        <div class="col-lg-5">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы уверены, что необходимо удалить #' . $model->id . ' заказ?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'total',
                    'total_with',
                    'created_at:datetime',
                    'updated_at:datetime',
                    'phone',
                    'email:email',
                    'notes:ntext',
                    'status',
                ],
            ]) ?>
        </div>
    </div>
    <h3><?= Html::encode('Перечень товаров заказа.') ?></h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
//            'order_id',
            'title',
            'price',
//            'product_id',
            'quantity',
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
