<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-lg-5">

        <div class="order-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(\common\models\Order::getStasusesList(), 'status', 'title')) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>