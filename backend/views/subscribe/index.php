<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\query\SubscribeQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подписка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать подписку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'email:email',
            'status',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',// {images}
            ],
        ],
    ]); ?>

</div>
