<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Rebate */

$this->title = 'Изменить скидки: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Скидки',   'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rebate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
