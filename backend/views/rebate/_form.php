<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Rebate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rebate-form">
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'action_id')->dropDownList(\common\models\Action::getActionList(), ['prompt' => 'Выбор акции']) ?>

                <?= $form->field($model, 'value')->textInput(['format' => 'integer']) ?>

                <?= $form->field($model, 'status')->dropDownList(\common\models\Rebate::getStatusesList(), ['prompt' => 'Выбор статуса']) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать скидку' : 'Изменить сидку', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>