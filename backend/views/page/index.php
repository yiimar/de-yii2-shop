<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Page;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'slug',
//            'route',
//            'meta_title',
//            'meta_keyword',
//            'meta_description',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'parent_id',
                'value'     => function ($model) {
                    return empty($model->parent_id) ? 'Меню' : HTML::encode(Page::getTitleById($model->parent_id));
                },
                'filter'    => Page::getParentList(),
            ],
//            'text:ntext',
            'status',

            [
                'class'         => 'yii\grid\ActionColumn',
                'header'        => 'Действия', 
                'headerOptions' => ['width' => '40'],
                'template'      => '{update} {delete}',//{view} 
                'buttons' => [
//                    'update' => function ($url,$model) {
//                        if ($model->parent_id !== null) {
//                            return Html::a(
//                                '<span class="glyphicon glyphicon-pencil"></span>',
//                                $url
//                            );
//                        }
//                    },
                    'delete' => function ($url,$model) {
                        if ($model->parent_id !== null) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                $url
                            );
                        }
                    },
                ],
            ],
        ],
    ]); ?>

</div>
