<?php

namespace backend\controllers;

use Yii;
use common\models\Slider;
use common\models\query\SliderQuery;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends \backend\components\BackendController
{
    const SLIDER_WIDTH = 1170;
    const SLIDER_HEIGHT = 450;

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderQuery();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slider();

        if (($model->load(Yii::$app->request->post())) && ($model->validate())) {

            $model->file  = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                $model->slide = $this->makeImage($model);
            }

            $model->save();
            return $this->redirect(['index']);

        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (($model->load(Yii::$app->request->post())) && ($model->validate())) {

            // get the instance of upload file
            $model->file  = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                $model->slide = $this->makeImage($model);
            }

            $model->save();
            return $this->redirect(['index']);

        } else {

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function makeImage($model)
    {
        $model->slide = $model->file->baseName . '.' . $model->file->extension;

        $model->file->saveAs(Yii::getAlias(
            '@sliderUploadPath/'
            . $model->slide
        ));

        $image = new \Imagick(Yii::getAlias('@sliderUploadPath/' . $model->slide));
        $image->cropThumbnailImage(self::SLIDER_WIDTH, self::SLIDER_HEIGHT);
        $model->slide = md5(time()) . '.' . $model->file->extension;
        $image->writeImage(Yii::getAlias('@sliderUploadPath/' . $model->slide));
        
        return $model->slide;
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
