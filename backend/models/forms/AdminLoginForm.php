<?php

namespace backend\models\forms;

use backend\models\Admin;
use Yii;
use yii\base\Model;

/**
 * AdminLoginForm is the model behind the login form.
 */
class AdminLoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username',  'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password',   'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'   => 'Имя пользователя',
            'password'   => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        ];
    }

    /**
     * Validates the username and password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Ошибочный пароль');
            } elseif ($user && $user->status == Admin::STATUS_BLOCKED) {
                $this->addError('username', 'Ошибочное имя пользователя');
            } elseif ($user && $user->status == Admin::STATUS_WAIT) {
                $this->addError('username', 'Профиль неподтвержден');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Admin::findByUsername($this->username);
        }

        return $this->_user;
    }
}
