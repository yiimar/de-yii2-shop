<?php

namespace common\components;

use yz\shoppingcart\DiscountBehavior;
/**
 * Description of DeboraDiscount
 *
 * @author yiimar
 */
class DeboraDiscount extends DiscountBehavior
{
    public $discountPercent  = 0;

    public function init($discountPercent)
    {
        $this->discountPercent = $discountPercent;
    }

    /**
     * @param CostCalculationEvent $event
     */
    public function onCostCalculation($event)
    {
        $event->discountValue = $this->discountPercent;
    }
}
