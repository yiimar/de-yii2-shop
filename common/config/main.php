<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',

    'charset' => 'utf-8',
    'timeZone' => 'Europe/Moscow',
    'language' => 'ru-Ru',

    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => ' ',
            'decimalSeparator'  => ',',
            'currencyCode'      => '',

            'dateFormat'     => 'dd-M-yyyy',
            'datetimeFormat' => 'dd-MM-yyyy, HH:mm:ss',
        ]
    ],
];
