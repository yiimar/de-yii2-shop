<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $slide
 * @property integer $page_id
 * @property integer $status
 */
class Slider extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE   = 1;
    const STATUS_DISABLE  = 0;
    const STATUS_ACTIVE_TEXT   = 'Показать';
    const STATUS_DISABLE_TEXT  = 'Скрыть';

    /**
     * File upload field
     * 
     * @var UploadedFile
     */
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'],           'integer'],
            [['title', 'slide',],   'string',   'max'   => 100],
            [['status'],            'in',       'range' => array_keys(self::getStasusesArray())],
            [['file'],              'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'title'   => 'Название',
            'slide'   => 'Картинка слайдера',
            'file'    => 'Загрузить Картинку',
            'page_id' => 'Страница',
            'status'  => 'Статус',
        ];
    }

    public static function getStatusesList()
    {
        $statuses = [
            [
                'id'     => self::STATUS_ACTIVE,
                'status' => self::STATUS_ACTIVE_TEXT,
            ],
            [
                'id'     => self::STATUS_DISABLE,
                'status' => self::STATUS_DISABLE_TEXT,
            ],
        ];
        return \yii\helpers\ArrayHelper::map($statuses, 'id', 'status');

    }

    public static function getStasusesArray()
    {
        return [
            self::STATUS_ACTIVE  => self::STATUS_ACTIVE_TEXT,
            self::STATUS_DISABLE => self::STATUS_DISABLE_TEXT,
        ];
    }

    public static function getTextByStatus($status)
    {
        $arr = self::getStasusesArray();
        return $arr[$status];
    }
}
