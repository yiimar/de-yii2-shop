<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%option}}".
 *
 * @property integer $id
 * @property string $attribute
 * @property string $value
 * @property string $status
 */
class Option extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE  = 'Активен';
    const STATUS_SUSPEND = 'Не активен';

    public static function Statuses()
    {
        return [
            [self::STATUS_ACTIVE],
            [self::STATUS_SUSPEND],
        ];
    }

    public static function getStatuses()
    {
        return [
            ['id' => self::STATUS_ACTIVE,  'title' => self::STATUS_ACTIVE],
            ['id' => self::STATUS_SUSPEND, 'title' => self::STATUS_SUSPEND],
        ];
    }
    public static function statusForDrop()
    {
        return \yii\helpers\ArrayHelper::map(self::getStatuses(), 'id', 'title');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute', 'value'], 'string', 'max' => 100],
            [['status'],             'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attribute' => 'Атрибут',
            'value'     => 'Значение',
            'status'    => 'Статус',
        ];
    }
}
