<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "picture".
 *
 * @property integer $id
 * @property integer $page_id
 *
 * @property Page $product
 */
class Picture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'picture';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'page_id' => 'Страница',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'product_id']);
    }

    /**
     * @return string image hash
     */
    protected function getHash()
    {
        return md5($this->page_id . '-' . $this->id);
    }

    /**
     * @return string path to image file
     */
    public function getPath()
    {
//        return Yii::getAlias('@backend/web/upload/' . $this->getHash() . '.jpg');
    }

    /**
     * @return string URL of the image
     */
    public function getUrl()
    {
//        return Yii::getAlias('@frontendWebroot/images/' . $this->getHash() . '.jpg');
    }

    public function afterDelete()
    {
        unlink($this->getPath());
        parent::afterDelete();
    }
}
