<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%social_link}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $url
 * @property string $class
 * @property string $status
 */
class SocialLink extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE  = 'Активна';
    const STATUS_SUSPEND = 'Не активна';

    public static function Statuses()
    {
        return [
            [self::STATUS_ACTIVE],
            [self::STATUS_SUSPEND],
        ];
    }

    public static function getStatuses()
    {
        return [
            ['id' => self::STATUS_ACTIVE,  'title' => self::STATUS_ACTIVE],
            ['id' => self::STATUS_SUSPEND, 'title' => self::STATUS_SUSPEND],
        ];
    }
    public static function statusForDrop()
    {
        return \yii\helpers\ArrayHelper::map(self::getStatuses(), 'id', 'title');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_link}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['label', 'url', 'class'], 'required'],
            [['label', 'url', 'class'], 'string', 'max' => 255],
            [['status'],                'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label'  => 'Наименование',
            'url'    => 'Ссылка',
            'class'  => 'Класс стиля',
            'status' => 'Статус',
        ];
    }
}
