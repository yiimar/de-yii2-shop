<?php

namespace common\models;

/**
 * Description of Category
 *
 * @author yiimar
 */
class Category
{
    const SERIA_FACE = 1;
    const SERIA_BODY = 2;
    const SERIA_HAIR = 3;
    const SERIA_FACE_TEXT = 'Серия для лица';
    const SERIA_BODY_TEXT = 'Серия для тела';
    const SERIA_HAIR_TEXT = 'Серия для волос';
    const SERIA_FACE_SLUG = 'seriya-dlya-litsa';
    const SERIA_BODY_SLUG = 'seriya-dlya-tela';
    const SERIA_HAIR_SLUG = 'seriya-dlya-volos';

    public static function getSlugArray()
    {
        return [
            self::SERIA_FACE => self::SERIA_FACE_SLUG,
            self::SERIA_BODY => self::SERIA_BODY_SLUG,
            self::SERIA_HAIR => self::SERIA_HAIR_SLUG,
        ];
    }

    public static function getSlugById($id)
    {
        return in_array((int) $id, array_keys(self::getSlugArray()))
                ? self::getSlugArray()[$id]
                : self::getSlugArray()[self::SERIA_FACE];
    }

    public static function getSeriaArray()
    {
        return [
            self::SERIA_FACE => self::SERIA_FACE_TEXT,
            self::SERIA_BODY => self::SERIA_BODY_TEXT,
            self::SERIA_HAIR => self::SERIA_HAIR_TEXT,
        ];
    }

    public static function getSeriaById($id)
    {
        return in_array((int) $id, array_keys(self::getSeriaArray()))
                ? self::getSeriaArray()[$id]
                : self::getSeriaById(self::SERIA_FACE);
    }

    public static function getIDBySeria($seria)
    {
        $arr = array_flip(self::getSeriaArray());
        
        return in_array($seria, array_keys($arr))
                ? $arr[$seria]
                : $arr(self::SERIA_FACE)[self::SERIA_FACE_TEXT];
        
    }

    public static function getSeriaList()
    {
        return [
            [
                'id'    => self::SERIA_FACE,
                'title' => self::SERIA_FACE_TEXT,
            ],
            [
                'id'    => self::SERIA_BODY,
                'title' => self::SERIA_BODY_TEXT,
            ],
            [
                'id'    => self::SERIA_HAIR,
                'title' => self::SERIA_HAIR_TEXT,
            ],
        ];
    }

    public static function makeSlug($slug)
    {
        switch ($slug) :
            case 'litsa':
            {
                $seria = [
                    self::SERIA_FACE,
                    self::SERIA_FACE_TEXT,
                ];
            }
            break;
        
            case 'tela':
            {
                $seria = [
                    self::SERIA_BODY,
                    self::SERIA_BODY_TEXT,
                ];
            }
            break;
            
            case 'volos':
            {
                $seria = [
                    self::SERIA_HAIR,
                    self::SERIA_HAIR_TEXT,
                ];
            }
            break;
        endswitch;
        
        return $seria;
    }
}
