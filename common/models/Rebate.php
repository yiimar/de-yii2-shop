<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%rebate}}".
 *
 * @property integer $id
 * @property integer $action_id
 * @property integer $value
 * @property string $status
 * @property integer $created
 * @property integer $closed
 */
class Rebate extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE   = 'Доступна';
    const STATUS_DISABLE  = 'Не доступна';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rebate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'value', 'status',],  'required'],
            [['action_id', 'value', 'created',], 'integer'],
            [['status'],                         'in', 'range' => self::getStasusesArray()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Вид скидки',
            'value'     => 'Размер скидки, %',
            'status'    => 'Статус',
            'created'   => 'Дата создания',
            'closed'    => 'Дата закрытия',
        ];
    }

    public static function getStatusesList()
    {
        $statuses = [
            [
                'status' => self::STATUS_ACTIVE,
                'status' => self::STATUS_ACTIVE,
            ],
            [
                'status' => self::STATUS_DISABLE,
                'status' => self::STATUS_DISABLE,
            ],
        ];
        return \yii\helpers\ArrayHelper::map($statuses, 'status', 'status');

    }

    public static function getStasusesArray()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_DISABLE,
        ];
    }

    public static function getRebateActual($action_id)
    {
        $model = self::find()
                ->where(['action_id' => $action_id])
                ->andWhere(['status' => self::STATUS_ACTIVE,])
                ->one();
        return $model->value;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created = time();
//                $this->status  = self::STATUS_ACTIVE;
                return true;
            }
        }
        return false;
    }
}
