<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\behaviors\SluggableBehavior;
use common\models\Category;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;
use common\components\SlugComponent;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property integer $category_id
 * @property string $price
 * @property string $photo
 * @property string $effect
 * @property string $short
 * @property string $bulk
 * @property string $advice
 * @property integer $advice_id
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 *
 * @property Image[] $images
 * @property OrderItem[] $orderItems
 * @property Category $category
 */
class Product extends \yii\db\ActiveRecord implements CartPositionInterface
{
    use CartPositionTrait;
    
    /**
     * File upload field
     * 
     * @var UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    public function behaviors()
    {
        return [
            [
                'class'     => SluggableBehavior::className(),
                'attribute' => 'title',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'meta_title', 'meta_keyword', 'meta_description'],      'string'],
            [['category_id'],        'in', 'range' => array_keys(Category::getSeriaArray())],
            [['advice_id', 'price'], 'integer'],
            [['title', 'slug', 'effect', 'short', 'bulk', 'advice', 'photo'],   'string', 'max' => 200],
            [['file',], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'title'       => 'Название',
            'slug'        => 'ЧПУ',
            'description' => 'Описание товара',
            'category_id' => 'Категория',
            'price'       => 'Цена',
            'photo'       => 'Картинка',
            'file'        => 'Картинка',
            'effect'      => 'Эффект',
            'short'       => 'Краткое описание',
            'bulk'        => 'Объем',
            'advice'      => 'Советы косметолога',
            'advice_id'   => 'Сопутствующий товар',
            'Quantity'    => 'Количество',
        ];
    }

    /**
     * @return Image[]
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['product_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    public static function getProductBySlug($slug)
    {
        if (($model = Product::findOne(['slug' => $slug])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public static function getTitleByID($id)
    {
        if (($model = self::findOne($id)) !== null)         return $model->title;
        else                                               return 'Нет такого товара';
    }

    public static function getTitleList()
    {
        $products = self::find()
                ->select(['id', 'title'])
                ->all();
        return \yii\helpers\ArrayHelper::map($products, 'id', 'title');
    }

//    public function beforeSave($insert = false)
//    {
//        if (parent::beforeSave($insert)) {
//            
////            if (($this->slug === null) && ($this->slug == '')) 
//                $this->slug = SlugComponent::make($this->title) . 's';
////                $n = 1;
////                while (Product::findOne(['slug' => $this->slug]) !== null) {
////                    $this->slug .= $n;
////                    $n++;
////                }
// 
//            return true;
//        }
//        return false;
//    }
}
