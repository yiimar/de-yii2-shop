<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%subscribe}}".
 *
 * @property integer $id
 * @property string $email
 * @property integer $status
 */
class Subscribe extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE  = 'Активна';
    const STATUS_SUSPEND = 'Отказ от подписки';

    private static $_length = 10;
    private static $_separ  = 6;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subscribe}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['email'],          'required', 'on' => ['create',]],
            [['code'],  'integer'],
            ['code',    'unique',   'targetClass' => self::className(), 'message' => 'Такой КОД уже зарегистрирован'],
            ['email',   'email'],
            ['email',   'unique',   'targetClass' => self::className(), 'message' => 'Такой EMAIL уже зарегистрирован'],
            ['email',   'string',   'length' => [5, 100]],
            ['status',  'string',   'length' => [5, 100]],
        ];
    }

    public static function statusesList()
    {
        return [
            self::STATUS_ACTIVE  => self::STATUS_ACTIVE,
            self::STATUS_SUSPEND => self::STATUS_SUSPEND,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'     => 'ID',
            'email'  => 'Электронная почта',
            'code'   => 'Код',
            'status' => 'Статус',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->isNewRecord) {
            $this->sendEmail($this);
        }
    }

    public function sendEmail()
    {
        return Yii::$app->mailer->compose('subscribe', ['model' => $this,])
            ->setTo($this->email)
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setSubject('Вы успешно подписаны на новости от Debora-Cosmetics')
            ->send();
    }

    public static function makeSuspend($id)
    {
        $model = self::find()
                ->where(['id' => $id])
                ->one();
        $model->status = self::STATUS_SUSPEND;
        
        return $model->save();
    }

    public static function resub($id)
    {
        $id =  (int) substr($id, 6, -4);//

        return self::makeSuspend($id);
    }

    public static function makeResubId($id)
    {
        if (is_integer($id) && ($id > 0)) {
            $min = 0;
            $max = 9;


            $str = '';
                for ($i = 0; $i < self::$_separ; $i++) :
                    $str .= rand($min, $max);
                endfor;

                $str .= $id;

                $hvost = self::$_length - self::$_separ;

                for ($i = 0; $i < $hvost; $i++) :
                    $str .= rand($min, $max);
                endfor;
            
            return $str;
            
        } else {
            return false;
        }
    }

    public function makeCoupon()
    {
        $min = 1;
        $max = 9;
        $length = 5;
        $res = '';
        for ($i = 0; $i < $length; $i++) {
            $res .= rand($min, $max);
        }
        
        return $res;
    }
}
