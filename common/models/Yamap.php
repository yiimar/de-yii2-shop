<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%yamap}}".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $url
 * @property string $sid
 * @property string $width
 * @property string $height
 * @property string $source_type
 * @property string $status
 */
class Yamap extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE  = 'Активна';
    const STATUS_SUSPEND = 'Не активна';

    public static function Statuses()
    {
        return [
            [self::STATUS_ACTIVE],
            [self::STATUS_SUSPEND],
        ];
    }

    public static function getStatuses()
    {
        return [
            ['id' => self::STATUS_ACTIVE,  'title' => self::STATUS_ACTIVE],
            ['id' => self::STATUS_SUSPEND, 'title' => self::STATUS_SUSPEND],
        ];
    }
    public static function statusForDrop()
    {
        return \yii\helpers\ArrayHelper::map(self::getStatuses(), 'id', 'title');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%yamap}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'sid'], 'required'],
            [['page_id'],        'integer'],
            [['url', 'sid', 'width', 'height', 'source_type'], 'string', 'max' => 100],
            [['status'],                                       'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id'     => 'Страница',
            'url'         => 'Url',
            'sid'         => 'Sid',
            'width'       => 'Width',
            'height'      => 'Height',
            'source_type' => 'SourceType',
            'status'      => 'Статус',
        ];
    }
}
