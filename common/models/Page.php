<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;


/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property integer $title
 * @property integer $slug
 * @property integer $route
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $parent_id
 * @property text $text
 * @property integer $status
 */
class Page extends \yii\db\ActiveRecord
{
    const STATUS_SHOW   = 'Видна';
    const STATUS_HIDDEN = 'Скрыта';

    protected $br_array = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    public function behaviors()
    {
        return [
            [
                'class'     => SluggableBehavior::className(),
                'attribute' => 'title',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title',], 'required'],
            [['created_at', 'updated_at', 'parent_id'], 'integer'],
            [['text'],   'string',],
            [
                [
                    'title', 'route', 'slug', 'meta_title', 'meta_keyword',
                    'meta_description', 'status'
                ],
                'string', 'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'title'             => 'Наименование',
            'slug'              => 'ЧПУ',
            'route'             => 'Путь',
            'meta_title'        => 'Meta_title',
            'meta_keyword'      => 'Meta Keywords',
            'meta_description'  => 'Meta Description',
            'created_at'        => 'Создана',
            'updated_at'        => 'Изменена',
            'parent_id'         => 'Родительская страница',
            'text'              => 'Текст',
            'status'            => 'Статус',
        ];
    }

    public static function getStatusesList()
    {
        $statuses = [
            ['id' => self::STATUS_SHOW,     'title' => self::STATUS_SHOW],
            ['id' => self::STATUS_HIDDEN,   'title' => self::STATUS_HIDDEN],
        ];
        return \yii\helpers\ArrayHelper::map($statuses, 'id', 'title');
    }

    public static function getParentList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()
                ->select(['id', 'title'])
                ->where(['parent_id' => null])
                ->all(),
                
                'id', 'title');
    }

    public static function getTotalList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()
                ->select(['id', 'title'])
                ->all(),
                
                'id', 'title');
    }

    public static function makeItemsForId($id)
    {
        $items = self::find()
                ->select(['title', 'slug'])
                ->where(['parent_id' => $id, 'status' => self::STATUS_SHOW])
                ->andWhere(['>', 'id', 5])
                ->all();
        return $items;
    }
    public static function getSlugById($id)
    {
        $model = self::find()->where(['id' => $id])->one();
        return $model->slug; 
    }

    public static function getBySlug($slug)
    {
        return self::findOne(['slug' => $slug]);
    }

    public static function getTitleById($id)
    {
        $model = self::findOne($id);
        
        if ($model)                     return $model->title;
        else                            return 'Страница не определена';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) :

            $this->updated_at = time();
            if ($this->isNewRecord)             $this->created_at = time();

            return true;
        endif;

        return false;
    }

    /**
     * Получение нодов дочернего уровня
     * 
     * @param type $id
     * @return type array
     */
    public static function getSubLevel($id)
    {
        return self::find()
                ->select(['id', 'parent_id', 'title', 'slug',])
                ->where(['parent_id' => $id])
                ->all();        
    }

    public static function getParentById($id)
    {
        $model  = self::findOne($id);
        $parent = self::findOne($model->parent_id);
        unset($model);
        
        return $parent;
    }

    /**
     * Получение всех нодов дочерней ветви, включая подветви
     * Массив используется для построения многоуровневых меню
     * 
     * @param type $id
     */
    public static function getBranch($id)
    {
        $branch = [];
        $nodes = self::getSubLevel($id);
        if (!empty($nodes)) {
            foreach ($nodes as $node) {
                $branch[$node->id] = self::getBranch($node->id);                
            }
        }
        return $branch;
    }

    /**
     * Получает массив определенных родителей для узла типа:
     *  [
     *      id, //свой
     *      id, //родителя
     *      id, //деда
     *      ...,
     *      id, //пра-пра-... деда
     *  ]
     * Этот массив используется для формирования исходных массивов для breadcrums
     * 
     * @staticvar array $line
     * @param type $id
     * @return type array
     */
    public static function getParentLine($id)
    {
        static $line = [];

        if (isset($id)) {
            $line[] = $id;
            $node = Node::find()->where(['id' => $id])->one();
            if ($node) {
                self::getParentLine($parent_id);
            }
        }

        return $line;
    }
}
