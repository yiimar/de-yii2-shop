<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

?>

<div class="top">
    <div class="container">
        <div class="header-table">
            <div class="top-nav">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mobile-menu" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="phone-block">
                <?php
                $option = common\models\Option::findOne(['status' => 'Активна', 'attribute' => 'Обратный звонок']);
                $phone  = $option->value;
                $option = common\models\Option::findOne(['status' => 'Активна', 'attribute' => 'Обратный звонок Подпись']);
                $link  = $option->value;
                ?>
                <div class="phone-num"><?= HTML::encode($phone); ?></div>
                <div><a class="callback" href="#" data-toggle="modal" data-target="#Callback"><?php echo HTML::encode($link); ?></a></div>
            </div>
            

            <div class="logo-block">
                <?php // $logoFactor = isset($this->params['logoFactor']) ? $this->params['logoFactor'] : [],?>
                <?php if ($this->params['logoLink'] === true) : ?><a href="/"><?php endif;// echo $this->title; ?>
                    <?php echo Html::img('@web/img/debora-logo.png', ['class' => 'logo-small',]) ?>
                    <?php echo Html::img('@web/img/logo-text.png',   ['class' => 'logo-text',]) ?>
                <?php if ($this->params['logoLink'] === true) : ?></a><?php endif; ?>
            </div>
                

                <?php
                $cart = \Yii::$app->cart; 
                    $num = 0;
                    if ($cart->getIsEmpty()) :
                        $res = HTML::encode('В корзине нет товаров');
                    else :
                        $num = $cart->getCount();
                        $sum = $cart->getCost();
                        $hvost = $num % 10;
                        switch ($hvost) :
                            case 1:
                                $hvost = '';
                                break;
                            case 2:
                            case 3:
                            case 4:
                                $hvost = 'а';
                                break;
                            default :
                                $hvost = 'ов';
                                break;                           
                        endswitch;
                        
                        if (in_array($num, [11,12,13,14,15,16,17,18,19]))   {$hvost='ов';}
                                
                        $res = HTML::encode($num . ' товар' . $hvost . ' на ' . $sum . ' р.');
                    endif;
                ?>
            <div class="basket-block">
                <a <?= ($cart->getIsEmpty()) ? 'onClick="return false"' : ''; ?> href="<?php echo Yii::$app->request->baseUrl . '/vasha-korzina/' ?>">
                    <?php echo Html::img('@web/img/basket-small.png', ['class' => 'basket-small',]) ?>

                    <div class="basket-q"  <?= ($cart->getIsEmpty()) ? 'style="display:none;"' : '' ?>>
                        <div class="q-number"><?php echo $num; ?></div>
                    </div>

                    <div class="basket-text">
                        <?= $res ?>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $headerMenu = require (Yii::$app->basePath . '/menu/headerMenu.php'); ?>
<div class="main-menu">
    <div class="container">
        <div class="collapse navbar-collapce" id="mobile-menu">
            <ul class="nav navbar-nav">
                
                <?php foreach ($headerMenu as $item) : ?>
                    <li>
                        <a href="<?php echo $item['url'] ?>">
                            <?php echo HTML::encode($item['label'])?>
                        </a>
                    </li>
                <?php endforeach ?>
                    
            </ul>
        </div>
    </div><!--container-->
</div>