<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = HTML::encode('Правый баннер');
$this->params['breadcrumbs'][] = $this->title;
$this->params['logoLink'] = true;
?>

<h1><?=Html::encode('Правый баннер')?></h1>