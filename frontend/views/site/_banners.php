<?php
use Yii;
use yii\helpers\Html;
use common\models\Banner;

?>

<div class="main-buttons">
    <?php $leftBanner = Banner::findOne([
        'title'         => 'Левый',
        'status'        => 'Показать',
        'place_page_id' => '6',
    ]); ?>
    <div class="main-buttons-l">
        <a href="<?=$leftBanner->target?>">
            <img 
                class="<?= $leftBanner->class ?>"
                src="<?= Yii::getAlias('@bannerUpload/') . $leftBanner->pict ?>"
            >
        </a>
    </div>
    <?php $rightBanner = Banner::findOne([
        'title'         => 'Правый',
        'status'        => 'Показать',
        'place_page_id' => '6',
    ]); ?>
    <div class="main-buttons-r">
        <a href="<?=$rightBanner->target?>">
            <img 
                class="<?=$rightBanner->class?>"
                src="<?=Yii::getAlias('@bannerUpload/') . $rightBanner->pict?>"
            >
        </a>
    </div>
</div>


