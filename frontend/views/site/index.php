<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Page;

$this->title = HTML::encode('Главная');
// Убираем ссылку с главного логотипа
$this->params['logoLink'] = false;
?>


<?php echo $this->render('_slider'); ?>

<?php
$page = Page::findOne(['status' => Page::STATUS_SHOW, 'title' => 'Главная']);
echo $page ?$page->text :'';
?>

<?php echo $this->render('_banners'); ?>			

<div class="sign-block">
    <span></span>
    <div class="sign-block-text">
        <div class="sign-text"><?php echo HTML::encode('Подпишитесь на новости и получите скидку 10% на Ваш первый заказ:'); ?></div>
    </div>

    <input id="subs-email" type="email" placeholder="<?php echo HTML::encode('Ваш email')?>">
    <button id="subs-button" class="dbr-btn"><?php echo HTML::encode('ПОДПИСАТЬСЯ')?></button>

    <?php $this->registerJsFile(
            'js/index.js',
            ['depends'=>'frontend\assets\AppAsset']
    );?>
</div>
