<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = HTML::encode('Левый баннер');
$this->params['breadcrumbs'][] = $this->title;
$this->params['logoLink'] = true;
?>

<h1><?=Html::encode('Левый баннер')?></h1>