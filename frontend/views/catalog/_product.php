<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Markdown;
use yii\bootstrap\Button;
use common\models\Category;
?>
<?php /** @var $model \common\models\Product */ ?>
<?php $url = Category::getSlugById($model->category_id) . '/' . $model->slug . '.html' ?>

<div class="good-item">

    <!--'catalog/view/' . $model->id-->
    <div class="good-holder">
        <a href="<?= Url::to([$url]) ?>">

            <div class="good-photo">
                <?php
                if (isset($model->photo)) {
                    echo Html::img(Yii::getAlias('@productUpload/' . $model->photo), ['width' => '100%']);
                }
                ?>
            </div>
        </a> 
             
            <div class="good-info-holder">
                <div class="good-title" style="height: 28px; overflow: hidden;"><a href="<?= Url::to([$url]) ?>"><?= Html::encode($model->title) ?></a></div>
                <div class="good-info"  style="height: 80px; overflow: hidden;">
                    <?= Markdown::process($model->description) ?>
                </div>
            </div>
            <div class="good-price"><?= Html::encode(Yii::$app->formatter->asInteger($model->price) . ' руб.') ?></div>
            
            <div class="add-to-basket">
                <button id="product-add" name="<?=$model->id?>" class="dbr-btn"><?= Html::encode('Добавить в корзину') ?></button>
            </div>
    </div>

</div>