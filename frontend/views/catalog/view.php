<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\models\Category;

/* @var $this yii\web\View */
$this->title = Html::encode($model->title);
$slug = Category::getSlugById($model->category_id);
$this->params['breadcrumbs'][] = [
    'label' => HTML::encode(Category::getSeriaById($model->category_id)),
    'url'   => Yii::$app->request->baseUrl . '/' . $slug . '/',
];
$this->params['breadcrumbs'][] = $this->title;

$this->params['logoLink'] = true;

if (isset($model->meta_title))          $this->params['title'] = $model->meta_title;
else                                    $this->params['title'] = $model->title;

$this->params['keywords']    = $model->meta_keyword;
$this->params['description'] = $model->meta_description;
?>

<h1><?= HTML::encode($model->title) ?></h1>

<?php if ($model->short) : ?>
    <div class="small-info"><?= HTML::encode($model->short) ?></div>
<?php endif ?>

<div class="card-holder">
    <div class="card">
        <div class="card-photo">
            <div class="card-zoom">
                <?php
                if (isset($model->photo)) {
                    echo Html::img(Yii::getAlias('@productUpload/' . $model->photo), ['width' => '100%']);
                }
                ?>
                <a href="#" class="pop" data-toggle="modal" data-target="#imagemodal"></a>
            </div>
        </div>
        <div class="price-info-holder">
            <div class="card-price-add">
                <div class="card-good-price"><?= Html::encode(Yii::$app->formatter->asInteger($model->price) . ' руб.') ?></div>
                <div class="card-add-to-basket">
                    <button name="<?=$model->id?>" class="dbr-btn">
                        <?= HTML::encode('Добавить в корзину') ?>
                    </button>
                </div>
            </div>
            <div class="card-info">
                <?php if ($model->effect) : ?>
                    <p class="card-subheader">Эффект</p>
                    <p><?= $model->effect ?></p>
                <?php endif ?>
                <?php if ($model->description) : ?>
                    <p class="card-subheader">Описание</p>
                    <p><?= HTML::encode($model->description) ?></p>
                <?php endif ?>
                <?php if ($model->bulk) : ?>
                    <p class="card-subheader">Объем</p>
                    <p><?= HTML::encode($model->bulk . ' мл') ?></p>
                <?php endif ?>
                <?php if ($model->advice) : ?>
                    <p class="card-subheader">Советы косметолога</p>
                    <p><?= HTML::encode($model->advice) ?></p> 
                    <p>Для усиления эффекта используйте </p>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content"  >              
      <div class="modal-body card-zoom-pop">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
        <img src="<?=Yii::getAlias('@productUpload/' . $model->photo)?>" class="imagepreview" width="500" height="auto">
      </div> 
    </div>
  </div>
</div>
