<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Html::encode($title);

$this->params['breadcrumbs'][] = $this->title;
$this->params['logoLink']      = true;
?>

<h1><?= $this->title ?></h1>

<div class="goods">
    <?php foreach ($productsQuery as $product) : ?>
        <?= $this->render('_product', ['model' => $product, 'title' => $this->title,]) ?>
    <?php endforeach ?>
</div>
