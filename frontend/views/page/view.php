<?php

/* @var $this yii\web\View */
use yii\helpers\Html;


$this->title = $model->title;

$this->params['breadcrumbs'][] = [
    'label' => HTML::encode($parent->title),
    'url'   => Yii::$app->request->baseUrl . '/' . $parent->slug . '/',
];
$this->params['breadcrumbs'][] = $this->title;

$this->params['logoLink'] = true;

if (isset($model->meta_title))          $this->params['title'] = $model->meta_title;
else                                    $this->params['title'] = $model->title;

$this->params['keywords']    = $model->meta_keyword;
$this->params['description'] = $model->meta_description;

?>

<div class="container">
    
    <h1><?php echo $model->title ?></h1>

    <?php echo $model->text; ?>
    <div style="display: block; width: 500px; height: 300px">
<script 
    type="text/javascript"
    charset="utf-8"
    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=z6HOoVJ4qHxERIcO-mKLWjfrNmYHSWhL&width=100%&height=400&lang=ru_RU&sourceType=constructor">
</script>
</div>
</div>

