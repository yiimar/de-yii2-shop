<?php

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $products common\models\Product[] */

$this->title = Html::encode('Ваша корзина');

$this->params['breadcrumbs'][] = $this->title;

$this->params['logoLink'] = true;
?>

<h1><?= HTML::encode($this->title) ?></h1>
<?php $cartNumber  = \Yii::$app->cart->getCount(); ?>
<?php if ($cartNumber > 0) : ?>
<div class="basket-holder">
    <div class="basket-left">
        <div class="goods">
            <?php foreach ($products as $product): ?>
                <div class="good-item">
                    <div class="good-holder">
                        <div class="good-photo">
                            <?php
                            if (isset($product->photo)) {
                                echo Html::img(Yii::getAlias('@productUpload/' . $product->photo), ['width' => '100%']);
                            }
                            ?>
                        </div>
                        <div class="good-info-holder">
                            <?php $category = common\models\Category::getSlugById($product->category_id); ?>
                            <div class="good-title"><a href="<?= "/$category/$product->slug.html" ?>"><?= HTML::encode($product->title) ?></a></div>
                            <div class="good-info"><?= Html::encode($product->short)?></div>
                        </div>
                        <?php // echo 'quantity= '.$product->quantity ?>
                        <div class="quantity">
                            <select title="<?=$product->getId()?>">
                                <?php for ($i = 1; $i <= 10; $i++) : ?>
                                    <option 
                                        value="<?= $i ?>"                                       
                                        <?php if ($product->quantity == $i) echo ' selected="selected"'; ?>
                                    >
                                        <?= $i ?>
                                    </option>
                                <?php endfor ?>
                            </select>
                        </div>
                        <div class="good-price"><?= Html::encode(Yii::$app->formatter->asInteger($product->price) . ' руб.') ?></div>
                    </div>
                    <div class="delete-item">
                        <a href="<?='/cart/remove/' . $product->getId() . '/'?>">
                            <?=HTML::encode('Удалить')?>
                        </a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <div class="basket-summ-block">
            <div class="basket-summ">
                <?php $cart = Yii::$app->cart ?>
                <?php $tot = $cart->getCost() ?>
                <?=  Html::encode('Сумма: ' . Yii::$app->formatter->asInteger($tot) . ' руб.') ?>
            </div>
            <div class="basket-sale">
                <?=  Html::encode('Скидка ' . $rebate . '%: ' . Yii::$app->formatter->asInteger($tot * $rebate) . ' руб.') ?>
            </div>
            <div class="basket-itogo">
                <?= Html::encode('Итого: ' .     Yii::$app->formatter->asInteger($cart->getCost(true)) . ' руб.') ?>
            </div>
        </div>
    </div>

    <div class="basket-right">

        <div class="basket-info">
            <?= Html::encode('Возникли вопросы или сложности с оформлением заказа? Мы готовы помочь! Позвоните в наш колл-центр по номеру 8 800 2000 000.')?>
        </div>

        <div class="promo">
            <div class="promo-header">
                <span><?= Html::encode('У вас есть промо-код?')?></span>
            </div>

            <input  id="promo-value"  type="text">
            <button id="promo-button" class="dbr-btn"><?= Html::encode('АКТИВИРОВАТЬ')?></button>
        </div>

    </div>
</div>

<div class="basket-form">
    <h4><?=HTML::encode('Оформление заказа')?></h4>
    <p><?=HTML::encode('Внимание! Все поля обязательны для заполнения.')?></p>

    <?php $form = ActiveForm::begin([]); ?>
        <div class="basket-inputs-holder">
            <div class="basket-form-inputs-left">

                <?php echo Html::activeInput('string', $order, 'fio',   ['placeholder' => HTML::encode('ФИО'),]); ?>
                <?php echo Html::activeInput('string', $order, 'phone', ['placeholder' => HTML::encode('Номер телефона'),]); ?>
                <?php echo Html::activeInput('string', $order, 'email', ['placeholder' => HTML::encode('Адрес электронной почты'),]); ?>
           
            </div>
            <div class="basket-form-inputs-right">
                <?php echo HTML::activeTextArea($order, 'address', ['placeholder' => HTML::encode('Адрес доставки'),]); ?>
            </div>
        </div>
        <div class="add-to-basket">
            <?= Html::submitButton(HTML::encode('Оформить заказ'), ['class' => 'dbr-btn']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>

<?php endif; ?>