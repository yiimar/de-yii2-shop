<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="basket-form">
    <h4><?=HTML::encode('Оформление заказа')?></h4>
    <p><?=HTML::encode('Внимание! Все поля обязательны для заполнения.')?></p>

    <?php $form = ActiveForm::begin([]); ?>
        <div class="basket-inputs-holder">
            <div class="basket-form-inputs-left">

                <?php echo Html::activeInput('string', $order, 'fio',   ['placeholder' => HTML::encode('ФИО'),]); ?>
                <?php echo Html::activeInput('string', $order, 'phone', ['placeholder' => HTML::encode('Номер телефона'),]); ?>
                <?php echo Html::activeInput('string', $order, 'email', ['placeholder' => HTML::encode('Адрес электронной почты'),]); ?>
           
            </div>
            <div class="basket-form-inputs-right">
                <?php echo HTML::activeTextArea($order, 'address', ['placeholder' => HTML::encode('Адрес доставки'),]); ?>
            </div>
        </div>
        <div class="add-to-basket">
            <?= Html::submitButton(HTML::encode('Оформить заказ'), ['class' => 'dbr-btn']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
