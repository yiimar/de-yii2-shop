$(document).ready(function () {

    $('#subs-button').click(function(){

        var url = '/site/subscribe';
        var email = $('input#subs-email');
        var emailV = email.val();

        if ((emailV == '') || !isValidEmailAddress(emailV)) {
            alert('Введите, пожалуйста, Ваш email корректно!');
            return false;
        };
        $.ajax({
            url: url,
            type: "post",
            data: {
                email: emailV,
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
//                var result = 'Вы подписаны- ' + emailV;
                email.val('');
                alert(data.str);
            },
        });
    });
    
    $('#callback-button').click(function(){

        var url = '/site/call';
        var phone = $('input#callback-phone');
        var phoneV = phone.val();
        var close = $('#callback-close');

        if ((phoneV == '') || !isValidPhoneNumber(phoneV)) {
            alert('Введите, пожалуйста, Ваш номер корректно!');
            return false;
        };
        
        $.ajax({
            url: url,
            type: "post",
            data: {
                phone: phoneV,
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                var res = 'Ваш заказ принят, мы перезвоним Вам в течение часа.';
                phone.val('');
                $("button#callback-close").trigger("click");
                alert(res);
            }
        });
    });

    $('button#product-add').click(function(){
        var id = $(this).attr('name');
        addToBasket(id);
        return false;
    });

    $('.card-add-to-basket button').click(function(){
        var id = $(this).attr('name');
        addToBasket(id);
        return false;
    });
    
    $('.quantity select').change(function(){
        
        var url      = '/korzina-izmen/';//
        var quantity = $(this).val();
        var id       = $(this).attr('title');

        $.ajax({
            url: url,
            type: "post",
            data: {
                id: id,
                quantity: quantity,
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function() {
                
            }
        });
    });

    $('#promo-button').click(function(){

        var url = '/korzina-promo/';
        var promo = $('input#promo-value');
        var promoVal = promo.val();

        if (promoVal.length != 10) {
            alert('Введите, пожалуйста, Ваш промо-код корректно!');
            return false;
        }

        $.ajax({
            url: url,
            type: "post",
            data: {
                promo: promoVal,
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                
            }
        });
    });

    function addToBasket(id) {
        var url    = '/cart/add';
        var basketQ = $('.basket-q')
        var qnumber = $('.q-number');
        var baskeTT = $('.basket-text');
        var basketL = $('.basket-block a');

        $.ajax({
            url: url,
            type: "post",
            data: {
                id: id,
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function(data) {
                basketQ.prop('style',null).off('style');
                qnumber.html(data.num);
                baskeTT.html(data.str);
                basketL.prop('onclick',null).off('click');
            }
        });
    }

    function isValidPhoneNumber(phoneV) {
        var pattern = new RegExp(/[\d-\s]{10,15}/);
        return pattern.test(phoneV);
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }
});


