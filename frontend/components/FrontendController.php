<?php

namespace frontend\components;

use yii\web\Controller;

/**
 * Description of FrontendController
 *
 * @author yiimar
 */
class FrontendController extends Controller
{
    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        
        $this->layout = 'front';
    }
    
}
