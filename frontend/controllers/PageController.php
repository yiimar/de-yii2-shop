<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontendController;
use common\models\Page;

/**
 * Description of PageController
 *
 * @author yiimar
 */
class PageController extends FrontendController
{
    public function actionConfidence()
    {
        $model = Page::findOne(3);
        return $this->render('section', ['model' => $model]);
    }

    public function actionTerms()
    {
        $model = Page::findOne(4);
        return $this->render('section', ['model' => $model]);
    }

    public function actionAbout()
    {
        $model = Page::findOne(1);
        return $this->render('section', ['model' => $model]);
    }

    public function actionShip()
    {
        $model = Page::findOne(2);
        return $this->render('section', ['model' => $model]);
    }

    public function actionContact()
    {
        $model = Page::findOne(5);
        return $this->render('section', ['model' => $model]);
    }

    public function actionView($slug)
    {
        $page = Page::getBySlug($slug);
        $parent = Page::findOne($page->parent_id);
        return $this->render('view', [
            'model'  => $page,
            'parent' => $parent,
        ]);
    }
}
