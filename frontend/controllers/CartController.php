<?php

namespace frontend\controllers;

use Yii;
use common\models\Order;
use common\models\OrderItem;
use common\models\Product;
use common\models\Category;
use common\models\Coupon;
use frontend\models\forms\RebateForm;
use frontend\components\FrontendController;
use yz\shoppingcart\ShoppingCart;

class CartController extends FrontendController
{
    public $rebate = null;
    public $promo  = null;
    public $enableCsrfValidation = false;

    public function actionAdd()
    {
        if (Yii::$app->request->isAjax && $data = Yii::$app->request->post()) {
            $id = explode(":", $data['id']);
            $id = $id[0];
            
            $product = Product::findOne($id);
            if ($product) {
                $cart = Yii::$app->cart;
                $cart->put($product);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'code' => 100,
                    'str'  => self::cartString(),
                    'num'  => $cart->getCount(),
                ];
            }
        }
    }

    public function actionUpdate()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $id = explode(":", $data['id']);
            $id = $id[0];
            
            $quantity = explode(":", $data['quantity']);
            $quantity = (int) $quantity[0];

            $product = Product::findOne($id);
            if ($product) {
                Yii::$app->cart->update($product, $quantity);
                
                $this->redirect('/vasha-korzina/');
            }
       }
    }

    public function actionPromo()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $promo = explode(":", $data['promo']);
            $promo = $promo[0];
            
            $this->redirect("/vasha-korzina/$promo/");
        }               
    }

    public function actionOrder($promo = null)
    {
        if ($promo !== null) {
            $this->promo  = $promo;
            $this->rebate = Coupon::getRebateByPromo($this->promo);
        }

        $order = new Order();

        $cart  = Yii::$app->cart;            /* @var $cart ShoppingCart */
        $products = $cart->getPositions();   /* @var $products Product[] */      
        
        if ($this->rebate) {
            $cart->on(ShoppingCart::EVENT_COST_CALCULATION, function ($event) {
                $event->discountValue = $this->rebate;
            });
        }

        if ($order->load(Yii::$app->request->post()) && $order->validate()) {
            
            $transaction = $order->getDb()->beginTransaction();
            
                $order->rebate      = $this->rebate;
                $order->total       = $cart->getCost();
                $order->total_with  = $cart->getCost(true);
                $order->save(false);
                $id = Yii::$app->db->getLastInsertID();

                foreach($products as $product) :
                    
                    $orderItem = new OrderItem();
                        $orderItem->order_id   = $order->id;
                        $orderItem->title      = $product->title;
                        $orderItem->price      = $product->getPrice();
                        $orderItem->product_id = $product->id;
                        $orderItem->quantity   = $product->getQuantity();

                    if (!$orderItem->save(false)) {
                        
                        $transaction->rollBack();
                        
                        Yii::$app->session->addFlash('error', 'Невозможно разместить заказ. Пожалйста, свяжитесь с нами!');
                        return $this->redirect('/');
                    }
                    
                endforeach;

            $transaction->commit();
            
            if ($promo !== null) {
                Coupon::makeClose($this->promo);
            }
            
            Yii::$app->cart->removeAll();

            Yii::$app->session->addFlash('success', 'Спасибо за Ваш заказ. Мы свяжемся с Вами в блтжайшее время!');
            
            $order->sendEmail();

            $model = Order::find()->where(['id' => $id])->one();
            $model->number = \common\components\OrderNumber::makeNumber($id);
            $model->save(false);

            return $this->redirect("/privetstvie/$model->number/");
        }

        return $this->render('order', [
            'order'    => $order,
            'products' => $products,
            'rebate'   => $this->rebate,
        ]);
    }

    public function actionRemove($id)
    {
        $product = Product::findOne($id);
        if ($product) {
            Yii::$app->cart->remove($product);
            $this->redirect('/vasha-korzina/');
        }
    }

    public function actionAdvert($zakaz)
    {
        return $this->render('advert', [
            'zakaz' => $zakaz,
        ]);
    }

    public static function cartString()
    {
        $cart = \Yii::$app->cart;
        $num = 0;
        if ($cart->getIsEmpty()) :
            $res = 'В корзине нет товаров';
        else :
            $num = $cart->getCount();
            $sum = $cart->getCost();
            $hvost = $num % 10;
            switch ($hvost) :
                case 1:
                    $hvost = '';
                    break;
                case 2:
                case 3:
                case 4:
                    $hvost = 'а';
                    break;
                default :
                    $hvost = 'ов';
                    break;
            endswitch;

            if (in_array($num, [11, 12, 13, 14, 15, 16, 17, 18, 19])) {
                $hvost = 'ов';
            }

            $res = $num . ' товар' . $hvost . ' на ' . $sum . ' р.';
        endif;

        return $res;
    }
}
