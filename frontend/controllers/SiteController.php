<?php
namespace frontend\controllers;

use Yii;
use frontend\components\FrontendController;
use frontend\models\forms\ContactForm;
use common\models\Subscribe;

use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionSubscribe()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            $model = new Subscribe();

            $email = explode(":", $data['email']);
            $email  = $email[0];
            
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (Subscribe::findOne(['email' => $email])) {
                return [
                    'code' => 100,
                    'str'  => 'Ваш Email уже подписан',
                ];
            } else {
                $model->code   = \common\models\Coupon::makeCode(1, $model->email);
                $model->status = Subscribe::STATUS_ACTIVE;
                $model->email  = $email;

                if ($model->validate() && $model->save()) {
                    $model->sendEmail();
                    sleep(1);
                
                    return [
                        'code' => 100,
                        'str' => $email . ' успешно подписан',
                    ];
                }
            }
        } else {
             throw new \yii\web\HttpException(404, 'Запрашиваемая страница не найдена');;
        }
    }

    public function actionLeftbanner()
    {
        return $this->render('leftbanner');
    }

    public function actionRightbanner()
    {
        return $this->render('rightbanner');
    }

    public function actionSend()
    {
        return $this->render('send');
    }

    public function actionResub($id)
    {
        $res = Subscribe::resub($id);

        return $this->render('resub', ['res' => $res,]);
    }

    public function actionCall()
    {
        $model = new \common\models\Callback();
        
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $phone = explode(":", $data['phone']);
            $model->phone = $phone[0];

            if (($model->validate()) && $model->save(false)) {
                
//                $this->send($model);
                
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;                
                return ['code'   => 100,];
            }
        }
    }
}
