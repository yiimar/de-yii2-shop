<?php

return [
    [
        'label' => 'Конфиденциальность',
        'url'   => '/konfidentsialnost/',
        'class' => 'item'
    ],
    [
        'label' => 'Условия использования',
        'url'   => '/usloviya-ispolzovaniya/',
        'class' => 'item'
    ],
    [
        'label' => 'Мы в социальных сетях:',
        'class' => 'last',
    ],
];

