<?php

use common\models\Category;

return [
    [
        'label' => 'О компaнии',
        'url'   => '/o-kompanii/',
        'class' => 'item'
    ],
    [
        'label' => 'Доставка и Оплата',
        'url'   => '/dostavka-i-oplata/',
        'class' => 'item'
    ],
    [
        'label' => Category::SERIA_FACE_TEXT,
        'url'   => '/' . Category::SERIA_FACE_SLUG . '/',
        'class' => 'item'
    ],
    [
        'label' => Category::SERIA_BODY_TEXT,
        'url'   => '/' . Category::SERIA_BODY_SLUG . '/',
        'class' => 'item'
    ],
    [
        'label' => Category::SERIA_HAIR_TEXT,
        'url'   => '/' . Category::SERIA_HAIR_SLUG . '/',
        'class' => 'item'
    ],
    [
        'label' => 'Контакты',
        'url'   => '/kontaktnaya-informatsiya/',//
        'class' => 'last',
    ],
];
